package autokeyvigenere

type key string

func formatKey(pt, keyin string) (key, bool) {
	p := key(formatGedeString(pt))
	formatedauto := key(formatGedeString(keyin))
	pnjlplt := len(p)
	v := formatedauto + p[0:pnjlplt-len(keyin)]
	return v, len(v) > 0
}

func enkrip(pt string, k key) string {
	ct := formatGedeString(pt)
	for index, char := range ct {
		ct[index] = 'A' + (char-'A'+k[index%len(k)]-'A')%26
	}
	return string(ct)
}

func dekrip(ct string, k key) (string, bool) {
	pt := make([]byte, len(ct))
	// pt := formatGedeString(ct)
	for charrloop := range pt {
		char := ct[charrloop]
		if char < 'A' || char > 'Z' {
			return "", false
		}
		pt[charrloop] = 'A' + (char-k[charrloop%len(k)]+26)%26
	}
	return string(pt), true
}

func formatGedeString(s string) []byte {
	arrout := make([]byte, 0, len(s))
	for charloop := 0; charloop < len(s); charloop++ {
		char := s[charloop]
		if char >= 'A' && char <= 'Z' {
			arrout = append(arrout, char)
		} else if char >= 'a' && char <= 'z' {
			arrout = append(arrout, char-32)
		}
	}
	return arrout
}

func Enc(key, pt string) string {
	v, status := formatKey(pt, key)
	if !status {
		return ""
	}
	ct := enkrip(pt, v)
	return ct
}

func Dec(key, ct, pt string) string {
	v, status := formatKey(pt, key)
	if !status {
		return ""
	}
	decripted, status := dekrip(ct, v)
	if !status {
		return ""
	}
	return decripted
}