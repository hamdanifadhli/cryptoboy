package vigenere

type vkey string

func formatKeyVig(key string) (vkey, bool) {
	v := vkey(formatGedeStringv(key))
	return v, len(v) > 0
}

func enkripVig(pt string, k vkey) string {
	ct := formatGedeStringv(pt)
	for index, char := range ct {
		ct[index] = 'A' + (char-'A'+k[index%len(k)]-'A')%26
	}
	return string(ct)
}

func dekripVig(ct string, k vkey) (string, bool) {
	pt := make([]byte, len(ct))
	// pt := formatGedeStringv(ct)
	for charrloop := range pt {
		char := ct[charrloop]
		if char < 'A' || char > 'Z' {
			return "", false
		}
		pt[charrloop] = 'A' + (char-k[charrloop%len(k)]+26)%26
	}
	return string(pt), true
}

func formatGedeStringv(s string) []byte {
	arrout := make([]byte, 0, len(s))
	for charloop := 0; charloop < len(s); charloop++ {
		char := s[charloop]
		if char >= 'A' && char <= 'Z' {
			arrout = append(arrout, char)
		} else if char >= 'a' && char <= 'z' {
			arrout = append(arrout, char-32)
		}
	}
	return arrout
}

func Enc(key, pt string) string {
	v, status := formatKeyVig(key)
	if !status {
		return ""
	}
	ct := enkripVig(pt, v)
	return ct
}

func Dec(key, ct string) string {
	v, status := formatKeyVig(key)
	if !status {
		return ""
	}
	decripted, status := dekripVig(ct, v)
	return decripted
}