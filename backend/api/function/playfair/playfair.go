package playfair

import (
	"fmt"
	"strings"
)
 
type option int
 
const (
    nope option = iota
    yes
)
 
type playfair struct {
    word string
    opsi     option
    table   [5][5]byte
}
 
func (p *playfair) start() {
    var used [26]bool 
    if p.opsi == nope {
        used[16] = true 
    } else {
        used[9] = true 
    }
    alphabet := strings.ToUpper(p.word) + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    for i, j, k := 0, 0, 0; k < len(alphabet); k++ {
        c := alphabet[k]
        if c < 'A' || c > 'Z' {
            continue
        }
        d := int(c - 65)
        if !used[d] {
            p.table[i][j] = c
            used[d] = true
            j++
            if j == 5 {
                i++
                if i == 5 {
                    break 
                }
                j = 0
            }
        }
    }
}
 
func (p *playfair) bersih (plainText string) string {
    plainText = strings.ToUpper(plainText)
    var cleanText strings.Builder
    prevByte := byte('\000')
    for i := 0; i < len(plainText); i++ {
        nextByte := plainText[i]
        if nextByte < 'A' || nextByte > 'Z' || (nextByte == 'Q' && p.opsi == nope) {
            continue
        }
        if nextByte == 'J' && p.opsi == yes {
            nextByte = 'I'
        }
        if nextByte != prevByte {
            cleanText.WriteByte(nextByte)
        } else {
            cleanText.WriteByte('X')
            cleanText.WriteByte(nextByte)
        }
        prevByte = nextByte
    }
    l := cleanText.Len()
    if l%2 == 1 {
        if cleanText.String()[l-1] != 'X' {
            cleanText.WriteByte('X')
        } else {
            cleanText.WriteByte('Z')
        }
    }
    return cleanText.String()
}
 
func (p *playfair) caribyte(c byte) (int, int) {
    for i := 0; i < 5; i++ {
        for j := 0; j < 5; j++ {
            if p.table[i][j] == c {
                return i, j
            }
        }
    }
    return -1, -1
}
 
func (p *playfair) encrypt(plainText string) string {
    cleanText := p.bersih (plainText)
    var cipherText strings.Builder
    l := len(cleanText)
    for i := 0; i < l; i += 2 {
        row1, col1 := p.caribyte(cleanText[i])
        row2, col2 := p.caribyte(cleanText[i+1])
        switch {
        case row1 == row2:
            cipherText.WriteByte(p.table[row1][(col1+1)%5])
            cipherText.WriteByte(p.table[row2][(col2+1)%5])
        case col1 == col2:
            cipherText.WriteByte(p.table[(row1+1)%5][col1])
            cipherText.WriteByte(p.table[(row2+1)%5][col2])
        default:
            cipherText.WriteByte(p.table[row1][col2])
            cipherText.WriteByte(p.table[row2][col1])
        }
        if i < l-1 {
            cipherText.WriteByte(' ')
        }
    }
    return cipherText.String()
}
 
func (p *playfair) decrypt(cipherText string) string {
    var decryptText strings.Builder
    l := len(cipherText)
    for i := 0; i < l; i += 3 {
        row1, col1 := p.caribyte(cipherText[i])
        row2, col2 := p.caribyte(cipherText[i+1])
        switch {
        case row1 == row2:
            temp := 4
            if col1 > 0 {
                temp = col1 - 1
            }
            decryptText.WriteByte(p.table[row1][temp])
            temp = 4
            if col2 > 0 {
                temp = col2 - 1
            }
            decryptText.WriteByte(p.table[row2][temp])
        case col1 == col2:
            temp := 4
            if row1 > 0 {
                temp = row1 - 1
            }
            decryptText.WriteByte(p.table[temp][col1])
            temp = 4
            if row2 > 0 {
                temp = row2 - 1
            }
            decryptText.WriteByte(p.table[temp][col2])
        default:
            decryptText.WriteByte(p.table[row1][col2])
            decryptText.WriteByte(p.table[row2][col1])
        }
        if i < l-1 {
            decryptText.WriteByte(' ')
        }
    }
    return decryptText.String()
}
 
func (p *playfair) Tabel() {
    fmt.Println("The table to be used is :\n")
    for i := 0; i < 5; i++ {
        for j := 0; j < 5; j++ {
            fmt.Printf("%c ", p.table[i][j])
        }
        fmt.Println()
    }
}
 
func Enc(key,q,pt string) string  {
	word := key
    ignoreQ := q
    opsi := nope
    if ignoreQ == "n" {
        opsi = yes
    }
    var table [5][5]byte
    pf := &playfair{word, opsi, table}
    pf.start()
    pf.Tabel()
	plainText := pt
	return pf.encrypt(plainText)
}

func Dec(key,q,ct string) string {
	word := key
    hiddentable := q
    opsi := nope
    if hiddentable == "n" {
        opsi = yes
    }
    var table [5][5]byte
    pf := &playfair{word, opsi, table}
    pf.start()
    pf.Tabel()
	encryptdText := ct
	return pf.decrypt(encryptdText)
}
