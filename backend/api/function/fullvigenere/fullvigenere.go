package fullvigenere

type keyf string

var alphabetBaru = [27]rune{'A', 'T', 'B', 'G', 'U', 'K', 'F', 'C', 'R', 'W', 'J', 'E', 'L', 'P', 'N', 'Z', 'M', 'Q', 'H', 'S', 'D', 'V', 'I', 'X', 'Y', 'O'}
var alpabet = [27]rune{'A', 'C', 'H', 'U', 'L', 'G', 'D', 'S', 'W', 'K', 'F', 'M', 'Q', 'O', 'Z', 'N', 'R', 'I', 'T', 'B', 'E', 'V', 'J', 'X', 'Y', 'P'}

func formatKey(key string) (keyf, bool) {
	v := keyf(formatGedeString(key))
	return v, len(v) > 0
}

func enkrip(pt string, k keyf) string {
	ct := formatGedeString(pt)
	for index, char := range ct {
		ct[index] = 'A' + (char-'A'+k[index%len(k)]-'A')%26
	}
	return string(ct)
}

func dekrip(ct string, k keyf) (string, bool) {
	pt := make([]byte, len(ct))
	// pt := formatGedeStringv(ct)
	for charrloop := range pt {
		char := ct[charrloop]
		if char < 'A' || char > 'Z' {
			return "", false
		}
		pt[charrloop] = realcheck('A' + (char-k[charrloop%len(k)]+26)%26)
	}
	return string(pt), true
}

func formatGedeString(s string) []byte {
	arrout := make([]byte, 0, len(s))
	for charloop := 0; charloop < len(s); charloop++ {
		char := s[charloop]
		char = byte(ordercheck(char))
		if char >= 'A' && char <= 'Z' {
			arrout = append(arrout, char)
		} else if char >= 'a' && char <= 'z' {
			arrout = append(arrout, char-32)
		}
	}
	return arrout
}

func ordercheck(c byte) byte {
	val := int(c)
	val = val - 65
	if c >= 'A' && c <= 'Z' {
		return byte(alphabetBaru[val])
	} else if c >= 'a' && c <= 'z' {
		val = val - 32
		return byte(alphabetBaru[val])
	} else {
		return 0
	}
}

func realcheck(c byte) byte {
	val := int(c)
	val = val - 65
	if c >= 'A' && c <= 'Z' {
		return byte(alpabet[val])
	} else if c >= 'a' && c <= 'z' {
		val = val - 32
		return byte(alpabet[val])
	} else {
		return 0
	}
}

func Enc(key, pt string) string {
	v, status := formatKey(key)
	if !status {
		return ""
	}
	ct := enkrip(pt, v)
	return ct
}

func Dec(key, ct string) string {
	v, status := formatKey(key)
	if !status {
		return ""
	}
	decripted, status := dekrip(ct, v)
	if !status {
		return ""
	}
	return decripted
}
