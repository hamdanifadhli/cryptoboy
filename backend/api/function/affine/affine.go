package affine

import (
	"strconv"
	"strings"
)


func enkrip(pt string, keyA int, keyB int) (string, bool) {
	ct := make([]byte, len(pt))
	value := 0
	for index := range ct {
		if (int(ct[index]) >= int('A')) || (int(pt[index]) <= int('Z')) {
			value = int(pt[index]) - int('A') + 1
			ct[index] = byte('A' + (value*keyA+keyB -1)%26)
		} else {
			if (int(pt[index]) >= int('A')) || (int(pt[index]) <= int('Z')) {
				value = int(pt[index]) - int('a') + 1
				ct[index] = byte('a' + (value*keyA+keyB -1)%26)
			} else {
				return "", false
			}
		}
	}
	return string(ct), true
}

func dekripConst(keyA int, n int) int {
	sisa := 0
	hasil := 0
	for {
	 	sisa = (sisa + keyA)%26
	 	hasil = hasil + 1
	 	if sisa%n == 1 {
	  		break
	 	}
	 	if hasil > n {
	  		if sisa%n != 1 {
	   			hasil = -1
			}
			break
	 	}
	}
	return hasil
}

func dekrip(ct string, keyA int, keyB int) (string, bool) {
	pt := make([]byte, len(ct))
	ptVary := 26 
	inverseKeyA := dekripConst(keyA, ptVary)
	for charrloop := range pt {
		char := ct[charrloop]
	 	if (char < 'A' || char > 'z' || inverseKeyA==-1 ) {
	  		return "", false
	 	} else {
	  		if (int(ct[charrloop]) >= int('A')) || (int(ct[charrloop]) <= int('Z')) {
	   			pt[charrloop] = byte((int('A') + ((((int(char) -int('A') +1- keyB+26) * inverseKeyA)) -1) % 26))
	  		} else {
	   			pt[charrloop] = byte((int('a') + ((((int(char) -int('a') +1 - keyB+26) * inverseKeyA)) -1) % 26))
	  		}
	 	}
	}
	return string(pt), true
}

func formatGedeString(s string) []byte {
	arrout := make([]byte, 0, len(s))
	for charloop := 0; charloop < len(s); charloop++ {
		char := s[charloop]
		if char >= 'A' && char <= 'Z' {
			arrout = append(arrout, char)
		} else if char >= 'a' && char <= 'z' {
			arrout = append(arrout, char-32)
		}
	}
	return arrout
}

func Enc(key,pt string) string {
	kunci := strings.Split(key,"-")
	keyA,err := strconv.Atoi(kunci[0])
	keyB,err := strconv.Atoi(kunci[1])
	pt = string(formatGedeString(pt))
	if err != nil {
		return ""
	}
	ct,status := enkrip(pt, keyA, keyB)
	if !status {
		return ""
	}
	return ct
}

func Dec(key,ct string) string {
	kunci := strings.Split(key,"-")
	keyA,err := strconv.Atoi(kunci[0])
	keyB,err := strconv.Atoi(kunci[1])
	ct = string(formatGedeString(ct))
	if err != nil {
		return ""
	}
	decripted, status := dekrip(ct, keyA, keyB)
	if !status {
		return ""
	}
	return decripted
}
