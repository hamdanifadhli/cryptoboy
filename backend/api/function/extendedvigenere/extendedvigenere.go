package extendedvigenere

import (
	"strconv"
	"strings"
)


type keyf string

func enkrip(pt string, k keyf) string {
	ct := formataja(pt)
	for index, char := range ct {
		ct[index] = (((k[index%len(k)] + char) - 1) % 255) + 1
	}
	return string(ct)
}

func dekrip(ct string, k keyf) (string, bool) {
	pt := formataja(ct)
	for charrloop := range pt {
		char := ct[charrloop]
		pt[charrloop] = (((char - k[charrloop%len(k)]) - 1) % 255) + 1
	}
	return string(pt), true
}

func formataja(s string) []byte {
	arrout := make([]byte, 0, len(s))
	for charloop := 0; charloop < len(s); charloop++ {
		char := s[charloop]
		arrout = append(arrout, char)
	}
	return arrout
}

func Enc(key, pt string) string {
	ptben := strings.Split(pt, "-")
	var sendpt []byte
	for i := 0; i < len(ptben); i++ {
		x, _ := strconv.Atoi(ptben[i])
		bet := byte(x)
		sendpt = append(sendpt,bet)
	}
	v := keyf(key)
	status := len(key) > 0
	if !status {
		return ""
	}
	ct := enkrip(string(sendpt), v)
	return ct
}

func Dec(key, ct string) string {
	ctben := strings.Split(ct, "-")
	var sendct []byte
	for i := 0; i < len(ctben); i++ {
		x, _ := strconv.Atoi(ctben[i])
		bet := byte(x)
		sendct = append(sendct,bet)
	}
	v := keyf(key)
	status := len(key) > 0
	if !status {
		return ""
	}
	decripted, status := dekrip(string(sendct), v)
	if !status {
		return ""
	}
	return decripted
}
