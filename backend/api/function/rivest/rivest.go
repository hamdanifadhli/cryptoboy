package rivest

import (
	"strconv"
	"strings"
)

// Swap dengan pointer agar pengaksesan array tidak redundant
func swapByte(val1 *byte, val2 *byte) {
	var temp = *val1
	*val1 = *val2
	*val2 = temp
}

func initArray() []byte {
	var initArr []byte
	for i := 0; i < 256; i++ {
		initArr= append(initArr,byte(i))
	}
	return initArr
}

// Key Scheduling Algo : Apa yang mau diubah
func ksa(K string, initArr []byte) {
	a := len(K)
	j := byte(0)
	for i := 0; i < 256; i++ {
		j = j + K[i%a] + initArr[i]
		swapByte(&initArr[i], &initArr[j])
	}
}

// Pseudo-Random Generation Array : Apa yang mau diubah
func prga(i *int, j *int, arrInit []byte, key string, charloop int) byte {
	var keyStream byte
	var a = *i
	var b = *j
	a = (a * int([]byte(key)[charloop % len(key)]) + 1) % 256 // Modifikasi dikalikan dengan key dan digeser satu ke kanan
	*i = a
	b = (b + int(arrInit[a])) % 256
	*j = b
	swapByte(&arrInit[a],&arrInit[b])
	var t = arrInit[a] + arrInit[b]
	keyStream = arrInit[t]
	return keyStream
}

// RC4 simetris shg stream dilakukan dengan fungsi yang sama

func enDec(ct string, arr []byte, key string) (string, bool) {
	var a int
	var b int
	var stream byte
	a = 0
	b = 0
	pt := formataja(ct)
	for charrloop := range pt {
		char := ct[charrloop]
		stream = prga(&a,&b, arr, key, charrloop)
		pt[charrloop] = char ^ stream
	}
	return string(pt), true
}

func formataja(s string) []byte {
	arrout := make([]byte, 0, len(s))
	for charloop := 0; charloop < len(s); charloop++ {
		char := s[charloop]
		arrout = append(arrout, char)
	}
	return arrout
}

// Extended Vigenere Shuffle Ksa Output
func extendedVig(pt string, k string) string {
	ct := formataja(pt)
	for index, char := range ct {
		ct[index] = (((k[index%len(k)] + char) - 1) % 255) + 1
	}
	return string(ct)
}
// Modifikasi Extended Vigenere Shuffle

// Enc export ke routing
func Enc(key, pt string) string {
	sendpt := containerByteInit(pt)
	var initArr []byte
	initArr = initArray()
	status := len(key) > 0
	ksa(key, initArr)

	initArr = []byte(extendedVig(string(initArr), key)) // Modifikasi

	ct, status := enDec(string(sendpt), initArr, key)
	if !status {
	 return ""
	}
	return ct
}

// Dec export ke routing
func Dec(key, ct string) string {
	sendct := containerByteInit(ct)
	var initArr []byte
	initArr = initArray()
	status := len(key) > 0
	ksa(key, initArr)
	
	initArr = []byte(extendedVig(string(initArr), key)) // Modifikasi
	
	pt, status := enDec(string(sendct), initArr, key)
	if !status {
	 return ""
	}
	return pt
}

func containerByteInit(s string) []byte {
	var temp []byte
	container := strings.Split(s,"-")
	for i := 0; i < len(container); i++ {
		intemp,_ := strconv.Atoi(container[i])
		bet := byte(intemp)
		temp = append(temp, bet)
	}
	return temp
}