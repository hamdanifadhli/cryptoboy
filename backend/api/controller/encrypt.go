package controller

import (
	"cyptoboy-backend/api/entity"
	"cyptoboy-backend/api/function/affine"
	"cyptoboy-backend/api/function/autokeyvigenere"
	"cyptoboy-backend/api/function/extendedvigenere"
	"cyptoboy-backend/api/function/fullvigenere"
	"cyptoboy-backend/api/function/playfair"
	"cyptoboy-backend/api/function/rivest"
	"cyptoboy-backend/api/function/vigenere"

	"github.com/gin-gonic/gin"
)

// EncryptController : Tipe kontrol Jenis Enkripsi
type EncryptController interface {
	CreateEncrypt(c *gin.Context) string
}
// CreateEncrypt : Kontrol Jenis Enkripsi
func CreateEncrypt(c *gin.Context) (string , []byte) {
	var encrypt entity.Encrypt
	var hehe []byte
	if err := c.ShouldBindJSON(&encrypt); err != nil {
		return "",hehe
	}
	if encrypt.Jenis == 0 {
		return "",hehe
	} else if encrypt.Jenis == 1 {
		return vigenere.Enc(encrypt.Key,encrypt.Plain),hehe
	} else if encrypt.Jenis == 2 {
		return fullvigenere.Enc(encrypt.Key,encrypt.Plain),hehe
	} else if encrypt.Jenis == 3 {
		return autokeyvigenere.Enc(encrypt.Key,encrypt.Plain),hehe
	} else if encrypt.Jenis == 4 {
		return "",[]byte(extendedvigenere.Enc(encrypt.Key,encrypt.Plain))
	} else if encrypt.Jenis == 5 {
		return playfair.Enc(encrypt.Key,"n",encrypt.Plain),hehe
	} else if encrypt.Jenis == 6 {
		return affine.Enc(encrypt.Key,encrypt.Plain),hehe
	} else if encrypt.Jenis == 7 {
		return "",[]byte(rivest.Enc(encrypt.Key,encrypt.Plain))
	} else {
		return "",hehe
	}
}

// CreateDecrypt : Kontrol Jenis Dekripsi
func CreateDecrypt(c *gin.Context) (string , []byte) {
	var decrypt entity.Decrypt
	var hehe []byte
	if err := c.ShouldBindJSON(&decrypt); err != nil {
		return "",hehe
	}
	if decrypt.Jenis == 0 {
		return "",hehe
	} else if decrypt.Jenis == 1 {
		return vigenere.Dec(decrypt.Key,decrypt.Cipher),hehe
	} else if decrypt.Jenis == 2 {
		return fullvigenere.Dec(decrypt.Key,decrypt.Cipher),hehe
	} else if decrypt.Jenis == 3 {
		return autokeyvigenere.Dec(decrypt.Key,decrypt.Cipher,decrypt.Plain),hehe
	} else if decrypt.Jenis == 4 {
		return "",[]byte(extendedvigenere.Dec(decrypt.Key,decrypt.Cipher))
	} else if decrypt.Jenis == 5 {
		return playfair.Dec(decrypt.Key,"n",decrypt.Cipher),hehe
	} else if decrypt.Jenis == 6 {
		return affine.Dec(decrypt.Key,decrypt.Cipher),hehe
	} else if decrypt.Jenis == 7 {
		return "",[]byte(rivest.Dec(decrypt.Key,decrypt.Cipher))
	} else {
		return "",hehe
	}
}

