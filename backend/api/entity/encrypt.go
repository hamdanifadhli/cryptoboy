package entity

type Encrypt struct {
	Jenis int    `json:"jenis"`
	Key   string `json:"key"`
	Plain string `json:"plain"`
}
