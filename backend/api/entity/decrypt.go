package entity

type Decrypt struct {
	Jenis  int    `json:"jenis"`
	Key    string `json:"key"`
	Cipher string `json:"cipher"`
	Plain  string `json:"plain"`
}
