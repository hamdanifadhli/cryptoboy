package routes

import (
	"cyptoboy-backend/api/controller"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

func convert( b []byte ) string {
    s := make([]string,len(b))
    for i := range b {
        s[i] = strconv.Itoa(int(b[i]))
    }
    return strings.Join(s,",")
}

func CreateEncrypt(c *gin.Context)  {
	res,res2 := controller.CreateEncrypt(c)
	if res!="" {
		c.JSON(200,gin.H{"msg":"sukses boy","data":res})
		} else if len(res2)>0 {
		out := convert(res2[:])
		c.JSON(200,gin.H{"msg":"sukses boy","data":out})
	} else {
		c.JSON(402,gin.H{"msg":"query failed, check your body please"})
	}
}

func CreateDecrypt(c *gin.Context)  {
	res,res2 := controller.CreateDecrypt(c)
	if res!="" {
		c.JSON(200,gin.H{"msg":"sukses boy","data":res})
	} else if len(res2)>0 {
		out := convert(res2[:])
		c.JSON(200,gin.H{"msg":"sukses boy","data":out})
	} else {
		c.JSON(402,gin.H{"msg":"query failed, check your body please"})
	}
}


func EncryptRoutes(router *gin.RouterGroup) {
	route := router.Group("/encrypt")
	route.POST("/", CreateEncrypt)
}

func DecryptRoutes(router *gin.RouterGroup) {
	route := router.Group("/decrypt")
	route.POST("/", CreateDecrypt)
}