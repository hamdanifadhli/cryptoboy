package main

import (
	"cyptoboy-backend/api"
	"cyptoboy-backend/config"
)


func main() {
	config.Init()
	api.Run()
}