# Cryptoboy
## _hamdani fadhli - 13217058_
## _gede satya adi dharma - 13217016_

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://www.linkedin.com/in/hamdani-fadhli-238811173/)

Tugas Kriptografi dan Koding Semester II 2020/2021. tugas ini dibuat untuk memenuhi kebutuhan tugas dari mata kuliah tersebut. software ini terdiri dari banyak sekali algoritma enkripsi dan dekripsi yang disatukan

teknologi yang gunakan adalah,
- Go Language di sisi Server
- React Framework di sisi Client
- Electron Js di wrapper application
- ✨Magic ✨

## Features
Fitur yang dapat berjalan dalam program ini adalah:
- dapat melakukan enkripsi dan juga dekripsi
- dapat melakukan enkripsi dalam bentuk text maupun file

dari semua yang sudah dijelaskan diatas dapat dilihat bahwa software sudah dapat melakukan enkripsi dan juga dekripsi dan juga sudah memiliki _ability_ untuk dapat di deploy ke cloud

akan dijelaskan juga panduan instalasi dan penggunaan dari software ini.

## Installation
pertama lakukan instalasi terhadap golang
> Code ini membutuhkan Go Lang untuk bekerja [golang](https://golang.org/) untuk berjalan.
> Masuk ke website golang dengan cara mengklik tulisan golang diatas ini. 
> Download golang tergantung terhadap _operating system_ kalian masing masing.
> Code ini juga membutuhkan Node JS dan juga penginstalan semua node module yang ada.

Clone
```bash
git clone this_git
```

kemudian setelah menginstall golang dapat dilakukan proses run terhadap codenya secara development mode.
```sh
cd Backend
go run main.go
```
golang sudah cukup pintar untuk menginstall semua depedency yang dibutuhkan secara otomatis tergantung terhadap isi code dan juga semua library yang kita gunakan.

untuk melakukan penginstalan di sisi client dapat dilakukan dengan cara.
```sh
cd Frontend
yarn add .
npm i
yarn
```

### Untuk Versi Production

kita dapat melakukan kompilasi terhadap code kita untuk menjadi executable file.

```sh
cd Backend
go build main.go
main.exe
```
```sh
cd Frontend
yarn built
```

semua code golang sudah berubah menjadi executable file yang dapat dijalankan kapanpun. file yang sudah tercompile juga akan disediakan dalam zip pengumpulan.

## Docker

Untuk melakukan deployment di sisi server, saya sudah mempersiapkan environtment docker yang akan melakukan restart secara automatis jika terjadi sebuah masalah pada server. deployment juga sudah sangat dimaksimalkan untuk menjalankannya dengan mudah dengan cara mengekspose port 8000 pada docker container terhadap port 8000 pada fisik server. untuk menjalankan docker ini juga sudah digunakan docker-compose yang jauh akan mempermudah kondisi deployment.

#### untuk windows

```sh 
cd Backend
docker-compose up
```

#### untuk linux (ubuntu family)

```sh 
cd Backend
sudo docker-compose up
```

pastikan port tidak digunakan. docker dan docker-compose jangan lupa untuk di install ya untuk memastikan dapat menjalankan server dalam kondisi dalam kontainer docker. dapat juga digunakan kubenertes pods atau cluster. tetapi mungkin untuk aplikasi sesederhana ini akan jauh lebih mudah jika kita menggunakan docker.

## Check

Menjalankan client.
```sh
cd Frontend
Yarn start
```

## Run Frontend

Start the frontend in the `dev` environment:

```bash
yarn start
```

## Build Frontend

To package Frontend for the local platform:

```bash
yarn package
```


## License

MIT

