export const judul = [
  'Standard Vigenere Cipher',
  'Full Vigenere Cipher',
  'Auto-key Vigenere Cipher',
  'Extended Vigenere Cipher',
  'Playfair Cipher',
  'Affine Cipher',
  'Modifikasi RC4',
];

export const deskripsi = [
  'Enkripsi ini memetakan plaintext dengan key dalam sebuah tabel pergeseran urutan alfabet.',
  'Enkripsi ini sama dengan Enkripsi Vigenere Standar, namun urutan alfabet diubah sesuai dengan PPT yang diberikan Pak Munir.',
  'Enkripsi ini sama dengan Enkripsi Vigenere Standar, namun key ditambah dengan plaintext.',
  'Enkripsi ini merupakan Enkripsi Vigenere dengan seluruh nilai ASCII, tidak terbatas pada alfabet saja.',
  'Enkripsi ini ditemukan oleh Sir Charles Wheatstone.',
  'Enkripsi ini merupakan Enkripsi yang melakukan multiplikasi serta pergeseran dari angka representasi alfabet.',
  'Enkripsi ini akan mengenkripsi File atau teks dengan Key Generator yang memiliki algoritma generasi kunci yang dimodifikasi. Modifikasi yang dilakukan dapat dilihat pada dokumentasi.',
];

export const pilihan = ['Masukkan Plaintext', 'Masukkan Ciphertext'];

export const proses = [
  'Mohon bersabar, karena proses enkripsi memakan waktu',
  'Mohon bersabar, karena proses dekripsi memakan waktu',
];

export const kelar = [
  'Proses enkripsi sudah selesai. Datamu sudah dirahasiakan. Ingat terus key-nya, ya!',
  'Proses dekripsi sudah selesai. Datamu sudah bisa dibaca kembali. Terima kasih!',
];

export const lihat = [
  'Silakan scroll untuk lihat hasil enkripsi. Kamu juga bisa download ciphertext menjadi file .txt. Bila kamu mengenkripsi dokumen, kamu belum bisa lihat isi ciphertext, karena format dokumen sudah berubah. Bila ini terjadi, dekripsikan dokumen itu.',
  'Silakan scroll untuk lihat hasil dekripsi. Kamu juga bisa download ciphertext menjadi file .txt. Bila kamu mendekripsi dokumen, lihat langsung dokumennya dan lihat apakah dokumen hasil sama dengan dokumen awal.',
];
