import React from 'react';
import clsx from 'clsx';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import { Home } from './page/P0_home';
import { PromptOp } from './page/P1_PromptOp';
import { PromptInput } from './page/P2_PromptInput';
import { PromptKey } from './page/P3_PromptKey';
import { Process } from './page/P4_Process';
import { Finished } from './page/P5_Finished';
import { AboutUs } from './page/P6_AboutUs';
import { MsgError } from './page/P7_MsgError';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: 'none',
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap',
    },
    drawerOpen: {
      backgroundColor: '#5762F2',
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      backgroundColor: '#5762F2',
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: theme.spacing(7) + 1,
      // [theme.breakpoints.up("sm")]: {
      //   width: theme.spacing(9) + 1
      // }
    },
    toolbar: {
      // display: 'flex',
      alignItems: 'center',
      // necessary for content to be below app bar
    },
    content: {
      flexGrow: 1,
    },
  })
);

export default function MiniDrawer() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [page, setPage] = React.useState(0);
  // Process enkripsi
  // 0 = belum ngapa-ngapain
  // 1 = minta data masukan
  // 2 = minta key
  // 3 = passing
  // 4 = selesai
  // -1 (exception) = about us
  const [process, setProcess] = React.useState(0);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const gantiPage = (popol: number) => {
    setPage(popol);
    console.log('cipher');
    console.log(popol, page);
  };

  const gantiProcess = (popol: number) => {
    setProcess(popol);
    console.log('stage:');
    console.log(popol, process);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
        style={{
          backgroundColor: 'transparent',
          margin: '0rem 1.4rem',
        }}
        elevation={0}
      >
        <Toolbar>
          {!open ? (
            <IconButton
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              className={clsx(classes.menuButton, {
                [classes.hide]: open,
              })}
            >
              <MenuIcon
                style={{
                  color: 'white',
                }}
              />
            </IconButton>
          ) : null}
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          {open ? (
            <IconButton
              onClick={handleDrawerClose}
              edge="start"
              style={{
                margin: '0',
                color: 'white',
              }}
            >
              <MenuIcon
                style={{
                  color: 'white',
                  margin: '0',
                  padding: '0',
                }}
              />
              <h4
                style={{
                  color: 'white',
                  margin: '0 0 0 2rem',
                  padding: '0 0 0 0',
                }}
              >
                Menu
              </h4>
            </IconButton>
          ) : (
            <IconButton
              onClick={handleDrawerClose}
              edge="start"
              style={{
                margin: '0 0 1.7rem 0',
              }}
            ></IconButton>
          )}
        </div>

        <Divider />
        <List
          style={{
            backgroundColor: '#5762F2',
          }}
        >
          {[
            'Standard Vigenere',
            'Full Vigenere',
            'Auto-key Vigenere',
            'Extended Vigenere',
            'Playfair',
            'Affine',
            'Rivest Cipher 4',
          ].map((text, index) => (
            <ListItem
              button
              key={text}
              onClick={() => {
                gantiPage(index * 2);
                setProcess(1);
              }}
            >
              <ListItemIcon>
                {index === 0 ? (
                  <h3
                    style={{
                      color: 'white',
                      margin: '0',
                      padding: '0',
                    }}
                  >
                    Vs
                  </h3>
                ) : index === 1 ? (
                  <h3
                    style={{
                      color: 'white',
                      margin: '0',
                      padding: '0',
                    }}
                  >
                    Fv
                  </h3>
                ) : index === 2 ? (
                  <h3
                    style={{
                      color: 'white',
                      margin: '0',
                      padding: '0',
                    }}
                  >
                    Av
                  </h3>
                ) : index === 3 ? (
                  <h3
                    style={{
                      color: 'white',
                      margin: '0',
                      padding: '0',
                    }}
                  >
                    Ev
                  </h3>
                ) : index === 4 ? (
                  <h3
                    style={{
                      color: 'white',
                      margin: '0',
                      padding: '0',
                    }}
                  >
                    Pf
                  </h3>
                ) : index === 5 ? (
                  <h3
                    style={{
                      color: 'white',
                      margin: '0',
                      padding: '0',
                    }}
                  >
                    Af
                  </h3>
                ) : (
                  <h3
                    style={{
                      color: 'white',
                      margin: '0',
                      padding: '0',
                    }}
                  >
                    R4
                  </h3>
                )}
              </ListItemIcon>
              <ListItemText
                primary={text}
                style={{
                  color: 'white',
                  margin: '0',
                  padding: '0',
                }}
              />
            </ListItem>
          ))}
        </List>
        <Divider />
        <List
          style={{
            backgroundColor: '#5762F2',
          }}
        >
          {['About Us'].map((text, index) => (
            <ListItem
              button
              key={text}
              onClick={() => {
                gantiPage(0);
                //About us
                setProcess(-1);
              }}
            >
              <ListItemIcon>
                {index === 0 ? (
                  <h3
                    style={{
                      color: 'white',
                      margin: '0',
                      padding: '0',
                    }}
                  >
                    Ab
                  </h3>
                ) : (
                  <MailIcon />
                )}
              </ListItemIcon>
              <ListItemText
                primary={text}
                style={{
                  color: 'white',
                  margin: '0',
                  padding: '0',
                }}
              />
            </ListItem>
          ))}
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {process === 0 ? (
          <Home />
        ) : process === 1 ? (
          <PromptOp
            Cipher={page}
            setCipher={(page) => gantiPage(page)}
            Stage={process}
            setStage={(process) => gantiProcess(process)}
          />
        ) : process === 2 ? (
          <PromptInput
            Cipher={page}
            Stage={process}
            setStage={(process) => gantiProcess(process)}
          />
        ) : process === 3 ? (
          <PromptKey
            Cipher={page}
            Stage={process}
            setStage={(process) => gantiProcess(process)}
          />
        ) : process === 4 ? (
          <Process
            Cipher={page}
            setCipher={(page) => gantiPage(page)}
            Stage={process}
            setStage={(process) => gantiProcess(process)}
          />
        ) : process === 5 ? (
          <Finished
            Cipher={page}
            setCipher={(page) => gantiPage(page)}
            Stage={process}
            setStage={(process) => gantiProcess(process)}
          />
        ) : process === 6 ? (
          <MsgError
            Cipher={page}
            setCipher={(page) => gantiPage(page)}
            Stage={process}
            setStage={(process) => gantiProcess(process)}
          />
        ) : (
          <AboutUs
            Cipher={page}
            setCipher={(page) => gantiPage(page)}
            Stage={process}
            setStage={(process) => gantiProcess(process)}
          />
        )}
      </main>
    </div>
  );
}
