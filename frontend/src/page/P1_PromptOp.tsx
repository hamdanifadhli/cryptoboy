import { Grid } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { Theme } from '@material-ui/core/styles';
import { createStyles } from '@material-ui/core/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';
import React from 'react';
import { deskripsi, judul } from '../constants/Text';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(0),
      textAlign: 'center',
    },
  })
);

interface PromptOpProps {
  Cipher: number;
  setCipher: (popol: number) => void;
  Stage: number;
  setStage: (popol: number) => void;
}

export function PromptOp(Props: PromptOpProps) {
  const classes = useStyles();
  const { Cipher, setCipher, Stage, setStage } = Props;
  return (
    <div
      style={{
        marginLeft: '2rem',
        marginRight: '2rem',
        flexGrow: 1,
      }}
    >
      <Grid container spacing={0}>
        <Grid
          item
          style={{
            height: '25vh',
          }}
          xs={12}
        >
          <h1>{judul[(Cipher - (Cipher % 2)) / 2]}</h1>
          <h4>{deskripsi[(Cipher - (Cipher % 2)) / 2]}</h4>
        </Grid>
        <Grid
          item
          style={{
            height: '12vh',
            textAlign: 'center',
          }}
          xs={12}
        >
          <h2>Apa yang akan kamu lakukan?</h2>
        </Grid>
        <Grid item xs={6}>
          <Paper
            onClick={() => {
              setStage(Stage + 1);
            }}
            className={classes.paper}
            style={{
              height: '40vh',
              backgroundColor: 'transparent',
              textAlign: 'center',
            }}
            elevation={0}
          >
            <img
              src="illustrations/encrypt.pnng.png"
              alt="skidi"
              style={{
                width: '20vw',
              }}
            />
            <h3>Encrypt</h3>
            <h4>Enkripsikan pesanmu agar bisa main rahasia-rahasiaan!</h4>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper
            className={classes.paper}
            onClick={() => {
              setCipher(Cipher + 1);
              setStage(Stage + 1);
            }}
            style={{
              height: '40vh',
              backgroundColor: 'transparent',
              textAlign: 'center',
            }}
            elevation={0}
          >
            <img
              src="illustrations/decrypt.png"
              alt="skidi"
              style={{
                width: '20vw',
              }}
            />
            <h3>Decrypt</h3>
            <h4>
              Dekripsikan kembali Ciphertext mu agar bisa melihat isi pesan
              sebenarnya!
            </h4>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
