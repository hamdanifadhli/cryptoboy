import { createStyles } from '@material-ui/core/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Button } from '@material-ui/core';
import Grid from '@material-ui/core/Grid/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import React from 'react';
import { deskripsi, judul, kelar, lihat } from '../constants/Text';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import FileSaver from 'file-saver';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(0),
      textAlign: 'center',
    },
  })
);

interface FinishedProps {
  Cipher: number;
  setCipher: (popol: number) => void;
  Stage: number;
  setStage: (popol: number) => void;
}

export function Finished(Props: FinishedProps) {
  const { Cipher, setCipher, Stage, setStage } = Props;
  const classes = useStyles();
  function byteToString(byte: string) {
    var x = byte.split(',').map((x) => +x);
    let bytesView = new Uint8Array(x);
    let str = new TextDecoder().decode(bytesView);
    return str;
  }
  return (
    <div
      style={{
        marginLeft: '2rem',
        marginRight: '2rem',
        flexGrow: 1,
      }}
    >
      <Grid container spacing={0}>
        <Grid
          item
          style={{
            height: '60vh',
            textAlign: 'center',

            paddingTop: '20vh',
          }}
          xs={12}
        >
          <img
            src="illustrations/sukses.png"
            alt="skidi"
            style={{
              height: '40vh',
            }}
          />
        </Grid>
        <Grid
          item
          style={{
            height: '20vh',
            textAlign: 'center',
            marginTop: '2vh',
          }}
          xs={12}
        >
          <h1>Hore</h1>
          <h3>{Cipher % 2 === 0 ? kelar[0] : kelar[1]}</h3>
          <h4>{Cipher % 2 === 0 ? lihat[0] : lihat[1]}</h4>
        </Grid>
        <Grid
          item
          style={{
            height: '10vh',
            textAlign: 'center',
            marginTop: '9vh',
          }}
          xs={12}
        >
          <h2>Hasil Pesan</h2>
        </Grid>
        {Cipher === 6 || Cipher === 7 ? (
          <Grid item xs={12}>
            <Paper
              className={classes.paper}
              style={{
                height: '45vh',
                padding: '0.25rem 1rem 0.25rem 1rem',
                textAlign: 'left',
                backgroundColor: '#FEB3B0',
              }}
              elevation={2}
            >
              <h3>
                Hasil disimpan, namun tidak bisa ditampilkan. Untuk enkripsi
                Extended Vigenere, silakan langsung Download Hasil.
              </h3>
            </Paper>
          </Grid>
        ) : (Cipher === 13 || Cipher === 12) &&
          localStorage.getItem('namafile') !== 'default.txt' ? (
          <Grid item xs={12}>
            <Paper
              className={classes.paper}
              style={{
                height: '45vh',
                padding: '0.25rem 1rem 0.25rem 1rem',
                textAlign: 'left',
                backgroundColor: '#FEB3B0',
              }}
              elevation={2}
            >
              <h3>
                Hasil disimpan, namun tidak bisa ditampilkan. Untuk enkripsi
                RC4, silakan langsung Download Hasil.
              </h3>
            </Paper>
          </Grid>
        ) : (
          <Grid item xs={12}>
            <Paper
              className={classes.paper}
              style={{
                height: '45vh',
                padding: '0.25rem 1rem 0.25rem 1rem',
                textAlign: 'left',
                backgroundColor: '#FEB3B0',
              }}
              elevation={2}
            >
              <h3>
                {Cipher === 12 || Cipher === 13
                  ? byteToString(localStorage.getItem('hasil'))
                  : localStorage.getItem('hasil')}
              </h3>
              {Cipher === 12 ? (
                <>
                  <p>Simpan angka dibawah ini untuk melakukan dekripsi</p>
                  {localStorage.getItem('hasil').replaceAll(',', ' ')}
                </>
              ) : (
                ''
              )}
            </Paper>
          </Grid>
        )}

        <Grid item xs={6}>
          <Paper
            className={classes.paper}
            style={{
              height: '10vh',
              marginTop: '3rem',
              textAlign: 'center',
              backgroundColor: 'transparent',
            }}
            elevation={0}
          >
            <Button
              style={{
                margin: '0 0 3rem 0',
                alignContent: 'center',
                height: '2.5rem',
                backgroundColor: '#FEB3B0',
              }}
              variant="contained"
              size="medium"
              component="span"
              onClick={() => {
                setStage(0);
              }}
            >
              Kembali ke Home
            </Button>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper
            className={classes.paper}
            style={{
              height: '10vh',
              marginTop: '3rem',
              textAlign: 'center',
              backgroundColor: 'transparent',
            }}
            elevation={0}
          >
            <Button
              style={{
                margin: '0 0 3rem 0',
                alignContent: 'center',
                height: '2.5rem',
                backgroundColor: '#FEB3B0',
              }}
              variant="contained"
              size="medium"
              component="span"
              onClick={() => {
                if (
                  (Cipher - (Cipher % 2)) / 2 === 3 ||
                  (Cipher - (Cipher % 2)) / 2 === 6
                ) {
                  var hasil = localStorage.getItem('hasil');
                  var types = localStorage.getItem('type');
                  var namafile = localStorage.getItem('namafile');
                  if (hasil != null && types != null && namafile != null) {
                    var hasilkirim = hasil.replace(/,/g, '-');
                    var be = hasilkirim.split('-').map(function (item: any) {
                      return Number(item);
                    });
                    if (Cipher % 2 === 0) {
                      var file = new File([hasilkirim], 'enc_' + namafile, {
                        type: types,
                      });
                    } else {
                      var file = new File(
                        [String.fromCharCode(...be)],
                        'dec_' + namafile.substr(4),
                        {
                          type: types,
                        }
                      );
                    }
                    FileSaver.saveAs(file);
                  }
                } else {
                  var hasil = localStorage.getItem('hasil');
                  var types = localStorage.getItem('type');
                  var namafile = localStorage.getItem('namafile');
                  if (hasil != null && types != null && namafile != null) {
                    var hasilkirim = hasil.replace(/,/g, '-');
                    var b = hasilkirim.split('-').map(function (item: string) {
                      return String(item);
                    });

                    if (Cipher % 2 === 0) {
                      var file = new File(b, 'enc_' + namafile, {
                        type: types,
                      });
                    } else {
                      var file = new File(
                        [hasilkirim],
                        'dec_' + namafile.substr(4),
                        {
                          type: types,
                        }
                      );
                    }
                    FileSaver.saveAs(file);
                  }
                }
              }}
            >
              Download
              {Cipher % 2 === 0 ? ' Ciphertext' : ' Plaintext'}
            </Button>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
