import { Grid } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { Theme } from '@material-ui/core/styles';
import { createStyles } from '@material-ui/core/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import React, { useState } from 'react';
import { deskripsi, judul, pilihan } from '../constants/Text';
import { Url, Header } from '../config/api';
import { FileInfoResult } from 'prettier';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(0),
      textAlign: 'center',
    },
  })
);

interface PromptInputProps {
  Cipher: number;
  Stage: number;
  setStage: (popol: number) => void;
}

export function PromptInput(Props: PromptInputProps) {
  const classes = useStyles();
  const { Cipher, Stage, setStage } = Props;
  const [input, setinput] = useState('');

  const handlefile = (val: any) => {
    if ((Cipher - (Cipher % 2)) / 2 === 3 && Cipher % 2 === 0) {
      var reader = new FileReader();
      var fileByteArray: number[] = [];
      var counter: number = 0;
      reader.readAsArrayBuffer(val.target.files[0]);
      reader.onloadend = function (evt: any) {
        if (evt.target.readyState == FileReader.DONE) {
          var arrayBuffer = evt.target.result,
            array = new Uint8Array(arrayBuffer);
          for (var i = 0; i < array.length; i++) {
            fileByteArray.push(array[i]);
            counter++;
          }
        }
        var bin: string = '';
        for (let x = 0; x < fileByteArray.length; x++) {
          bin = bin + String(fileByteArray[x]) + '-';
        }
        localStorage.setItem('namafile', val.target.files[0].name);
        localStorage.setItem('type', val.target.files[0].type);
        localStorage.setItem('plaintext', bin.slice(0, bin.length - 1));
        localStorage.setItem('jenis', String(Cipher / 2 + 1));
        setStage(Stage + 1);
      };
    } else if ((Cipher - (Cipher % 2)) / 2 === 3 && Cipher % 2 === 1) {
      let file = val.target.files[0];
      let reader = new FileReader();
      reader.readAsText(file);
      reader.onload = function () {
        if (reader.result != null) {
          localStorage.setItem('namafile', val.target.files[0].name);
          localStorage.setItem('type', val.target.files[0].type);
          localStorage.setItem('ciphertext', String(reader.result));
          localStorage.setItem('jenis', String((Cipher - 1) / 2 + 1));
        }
        setStage(Stage + 1);
      };
      reader.onerror = function () {
        console.log(reader.error);
      };
    } else {
      let file = val.target.files[0];
      let reader = new FileReader();
      reader.readAsText(file);
      reader.onload = function () {
        if (Cipher % 2 === 0) {
          localStorage.setItem('namafile', val.target.files[0].name);
          localStorage.setItem('type', val.target.files[0].type);
          localStorage.setItem('plaintext', String(reader.result));
          localStorage.setItem('jenis', String(Cipher / 2 + 1));
        } else {
          localStorage.setItem('namafile', val.target.files[0].name);
          localStorage.setItem('type', val.target.files[0].type);
          localStorage.setItem('ciphertext', String(reader.result));
          localStorage.setItem('jenis', String((Cipher - 1) / 2 + 1));
        }
        setStage(Stage + 1);
      };
      reader.onerror = function () {
        console.log(reader.error);
      };
    }
  };

  const handlesubmit = () => {
    if (Cipher === 13) {
      var out_send = input.replaceAll(' ', '-');
      localStorage.setItem('namafile', 'default.txt');
      localStorage.setItem('type', 'text/plain');
      localStorage.setItem('ciphertext', out_send);
      localStorage.setItem('jenis', String((Cipher - 1) / 2 + 1));
    } else if (Cipher % 2 === 0) {
      localStorage.setItem('namafile', 'default.txt');
      localStorage.setItem('type', 'text/plain');
      localStorage.setItem('plaintext', input);
      localStorage.setItem('jenis', String(Cipher / 2 + 1));
    } else {
      localStorage.setItem('namafile', 'default.txt');
      localStorage.setItem('type', 'text/plain');
      localStorage.setItem('ciphertext', input);
      localStorage.setItem('jenis', String((Cipher - 1) / 2 + 1));
    }
    setStage(Stage + 1);
  };

  const handlechange = (val: string) => {
    setinput(val);
  };
  return (
    <div
      style={{
        marginLeft: '2rem',
        marginRight: '2rem',
        flexGrow: 1,
      }}
    >
      <Grid container spacing={0}>
        <Grid
          item
          style={{
            height: '25vh',
          }}
          xs={12}
        >
          <h1>{judul[(Cipher - (Cipher % 2)) / 2]}</h1>
          <h4>{deskripsi[(Cipher - (Cipher % 2)) / 2]}</h4>
        </Grid>
        <Grid
          item
          style={{
            height: '12vh',
            textAlign: 'center',
          }}
          xs={12}
        >
          <h2>{Cipher % 2 === 0 ? pilihan[0] : pilihan[1]}</h2>
        </Grid>
        <Grid item xs={12}>
          <Paper
            className={classes.paper}
            style={{
              height: '35vh',
              backgroundColor: 'transparent',
              textAlign: 'center',
            }}
            elevation={0}
          >
            <TextField
              rows={12}
              label={Cipher % 2 === 0 ? pilihan[0] : pilihan[1]}
              placeholder="Teks"
              multiline
              variant="outlined"
              style={{
                width: '95%',
              }}
              value={input}
              onChange={(e) => handlechange(e.target.value)}
            />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper
            className={classes.paper}
            style={{
              height: '10vh',
              marginTop: '3rem',
              textAlign: 'center',
              backgroundColor: 'transparent',
              flexDirection: 'row',
              display: 'flex',
            }}
            elevation={0}
          >
            {/* <img
              src="illustrations/decrypt.png"
              alt="skidi"
              style={{
                height: "30vh",
              }}
            /> */}
            <Button
              style={{
                marginLeft: '1.5rem',
                marginTop: '0.5rem',
                height: '2.5rem',
                backgroundColor: '#FEB3B0',
              }}
              variant="contained"
              size="medium"
              component="span"
              disabled={input === ''}
              onClick={handlesubmit}
            >
              LANJUT
            </Button>
            {/* Simpan input */}
            <h4
              style={{
                marginLeft: '1rem',
                marginRight: '1rem',
              }}
            >
              atau masukkan file
            </h4>
            <input
              //   accept=".jpg, .pdf, .docx,.txt"
              style={{
                display: 'none',
              }}
              id="contained-button-file"
              multiple
              type="file"
              onChange={(e) => handlefile(e)}
            />
            <label htmlFor="contained-button-file">
              <Button
                style={{
                  marginTop: '0.5rem',
                  height: '2.5rem',
                  backgroundColor: '#FEB3B0',
                  color: 'black',
                }}
                variant="contained"
                color="primary"
                component="span"
              >
                UPLOAD
              </Button>
            </label>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
