import { Grid } from "@material-ui/core";
import Button from "@material-ui/core/Button/Button";
import Paper from "@material-ui/core/Paper";
import { Theme } from "@material-ui/core/styles";
import { createStyles } from "@material-ui/core/styles";
import makeStyles from "@material-ui/core/styles/makeStyles";
import React from "react";
import { deskripsi, judul } from "../constants/Text";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(0),
      textAlign: "center",
    },
  })
);

interface AboutUsProps {
  Cipher: number;
  setCipher: (popol: number) => void;
  Stage: number;
  setStage: (popol: number) => void;
}

export function AboutUs(Props: AboutUsProps) {
  const classes = useStyles();
  const { Cipher, setCipher, Stage, setStage } = Props;
  return (
    <div
      style={{
        marginLeft: "2rem",
        marginRight: "2rem",
        flexGrow: 1,
      }}
    >
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <h1>About Cryptoboy</h1>
          <h4>
            Cryptoboy merupaka project program kriptografi yang disusun oleh
            Hamdani Fadhli dan Gede Satya Adi Dharma, mahasiswa Teknik Elektro
            2017, dalam pemenuhan tugas Mata Kuliah Kriptografi.
          </h4>
        </Grid>
        <Grid container spacing={2}>
          {" "}
          <Grid item xs={6}>
            <Paper
              className={classes.paper}
              style={{
                width: "25vw",
                backgroundColor: "#90B1FA",
                marginLeft: "20%",
                textAlign: "center",
                paddingBottom: "1vh",
              }}
              elevation={2}
            >
              <img
                src="photos/hamdani.jpg"
                alt="skidi"
                style={{
                  width: "20vw",
                  marginTop: "2vh",
                }}
              />
              <h3>Hamdani</h3>
              <h4>13217058</h4>
            </Paper>
          </Grid>
          <Grid item xs={6}>
            <Paper
              className={classes.paper}
              style={{
                width: "25vw",
                backgroundColor: "#90B1FA",
                marginRight: "20%",
                textAlign: "center",
                paddingBottom: "1vh",
              }}
              elevation={2}
            >
              <img
                src="photos/satya.jpg"
                alt="skidi"
                style={{
                  width: "20vw",
                  marginTop: "2vh",
                }}
              />
              <h3>Satya</h3>
              <h4>13217016</h4>
            </Paper>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Paper
            className={classes.paper}
            style={{
              height: "10vh",
              marginTop: "3rem",
              textAlign: "center",
              backgroundColor: "transparent",
              flexDirection: "row",
              display: "flex",
            }}
            elevation={0}
          ></Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper
            className={classes.paper}
            style={{
              height: "10vh",
              marginTop: "3rem",
              textAlign: "center",
              backgroundColor: "transparent",
              flexDirection: "row",
              display: "flex",
            }}
            elevation={0}
          >
            <Button
              style={{
                marginLeft: "1.5rem",
                marginTop: "0.5rem",
                height: "2.5rem",
                backgroundColor: "#FEB3B0",
              }}
              variant="contained"
              size="medium"
              component="span"
              onClick={() => {
                setStage(0);
              }}
            >
              Kembali ke Home
            </Button>
            {/* Simpan input */}
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
