import { createStyles } from '@material-ui/core/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Button } from '@material-ui/core';
import Grid from '@material-ui/core/Grid/Grid';
import Paper from '@material-ui/core/Paper';
import React from 'react';
import { Theme } from '@material-ui/core/styles/createMuiTheme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(0),
      textAlign: 'center',
    },
  })
);

interface MsgErrorProps {
  Cipher: number;
  setCipher: (popol: number) => void;
  Stage: number;
  setStage: (popol: number) => void;
}

export function MsgError(Props: MsgErrorProps) {
  const { Cipher, setCipher, Stage, setStage } = Props;
  const classes = useStyles();
  return (
    <div
      style={{
        marginLeft: '2rem',
        marginRight: '2rem',
        flexGrow: 1,
      }}
    >
      <Grid container spacing={0}>
        <Grid
          item
          style={{
            height: '60vh',
            textAlign: 'center',

            paddingTop: '20vh',
          }}
          xs={12}
        >
          <img
            src="illustrations/eror.png"
            alt="skidi"
            style={{
              height: '40vh',
            }}
          />
        </Grid>
        <Grid
          item
          style={{
            height: '20vh',
            textAlign: 'center',
            marginTop: '2vh',
          }}
          xs={12}
        >
          <h1>Sepertinya ada masalah</h1>
          <h4>
            Coba lakukan kembali enkripsi atau dekripsi dengan format yang lebih
            baik. Lebih teliti lagi, ya!
          </h4>
        </Grid>

        <Grid item xs={12}>
          <Paper
            className={classes.paper}
            style={{
              height: '10vh',
              marginTop: '3rem',
              textAlign: 'center',
              backgroundColor: 'transparent',
              flexDirection: 'row',
              display: 'flex',
            }}
            elevation={0}
          >
            <Button
              style={{
                marginLeft: '1.5rem',
                marginTop: '0.5rem',
                height: '2.5rem',
                backgroundColor: '#FEB3B0',
              }}
              variant="contained"
              size="medium"
              component="span"
              onClick={() => {
                setStage(0);
              }}
            >
              Kembali ke Home
            </Button>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
