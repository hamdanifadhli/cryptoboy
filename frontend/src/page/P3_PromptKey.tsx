import { Grid } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { Theme } from '@material-ui/core/styles';
import { createStyles } from '@material-ui/core/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';
import TextField from '@material-ui/core/TextField';
import { PlaylistAddOutlined } from '@material-ui/icons';
import React, { useState } from 'react';
import { deskripsi, judul } from '../constants/Text';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(0),
      textAlign: 'center',
    },
  })
);

interface PromptKeyProps {
  Cipher: number;
  Stage: number;
  setStage: (popol: number) => void;
}

export function PromptKey(Props: PromptKeyProps) {
  const classes = useStyles();
  const [key, setkey] = useState('');
  const [plain, setplain] = useState('');

  const handlesubmit = () => {
    const pt = localStorage.getItem('plaintext');
    if (key) {
      if (pt !== null) {
        localStorage.setItem('plaindec', plain);
        localStorage.setItem('key', key);
        setStage(Stage + 1);
      } else {
        setStage(Stage - 1);
      }
    }
  };

  const handleinput = (key: string) => {
    setkey(key);
  };

  const handleinputplain = (pt: string) => {
    setplain(pt);
  };

  const { Cipher, Stage, setStage } = Props;

  return (
    <div
      style={{
        marginLeft: '2rem',
        marginRight: '2rem',
        flexGrow: 1,
      }}
    >
      <Grid container spacing={0}>
        <Grid
          item
          style={{
            height: '25vh',
          }}
          xs={12}
        >
          <h1>{judul[(Cipher - (Cipher % 2)) / 2]}</h1>
          <h4>{deskripsi[(Cipher - (Cipher % 2)) / 2]}</h4>
        </Grid>
        <Grid
          item
          style={{
            height: '12vh',
            textAlign: 'center',
          }}
          xs={12}
        >
          <h2>Masukkan key yang akan digunakan</h2>
        </Grid>
        {Cipher === 5 ? (
          <Grid container xs={12} spacing={0}>
            <Grid item xs={6}>
              <Paper
                className={classes.paper}
                style={{
                  height: '35vh',
                  backgroundColor: 'transparent',
                  textAlign: 'center',
                }}
                elevation={0}
              >
                <TextField
                  rows={12}
                  label="Masukkan key"
                  placeholder="Teks"
                  multiline
                  value={key}
                  onChange={(e) => handleinput(e.target.value)}
                  variant="outlined"
                  style={{
                    width: '95%',
                  }}
                />
              </Paper>
            </Grid>
            <Grid item xs={6}>
              <Paper
                className={classes.paper}
                style={{
                  height: '35vh',
                  backgroundColor: 'transparent',
                  textAlign: 'center',
                }}
                elevation={0}
              >
                <TextField
                  rows={12}
                  label="Masukkan Plaintext"
                  placeholder="Teks"
                  multiline
                  value={plain}
                  onChange={(e) => handleinputplain(e.target.value)}
                  variant="outlined"
                  style={{
                    width: '95%',
                  }}
                />
              </Paper>
            </Grid>
          </Grid>
        ) : (
          <Grid item xs={12}>
            <Paper
              className={classes.paper}
              style={{
                height: '35vh',
                backgroundColor: 'transparent',
                textAlign: 'center',
              }}
              elevation={0}
            >
              <TextField
                rows={12}
                label="Masukkan key"
                placeholder="Teks"
                multiline
                variant="outlined"
                value={key}
                onChange={(e) => handleinput(e.target.value)}
                style={{
                  width: '95%',
                }}
              />
            </Paper>
          </Grid>
        )}
        <Grid item xs={12}>
          <Paper
            className={classes.paper}
            style={{
              height: '10vh',
              marginTop: '3rem',
              textAlign: 'center',
              backgroundColor: 'transparent',
              flexDirection: 'row',
              display: 'flex',
            }}
            elevation={0}
          >
            <Button
              style={{
                marginLeft: '1.5rem',
                marginTop: '0.5rem',
                height: '2.5rem',
                backgroundColor: '#FEB3B0',
              }}
              variant="contained"
              size="medium"
              disabled={Cipher === 5 ? key === '' || plain === '' : key === ''}
              component="span"
              onClick={handlesubmit}
            >
              LANJUT
            </Button>
            {/* Simpan input */}
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
